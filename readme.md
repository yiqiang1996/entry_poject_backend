# Shopee Entry Project Backend
==========================================

## Environment Required

- node V14.15.4
- mongodb installed and running on localhost:27017 without authentification
- **or** you can go to **server.js** and change the mongodb connection url.

## Command to Start the Program 

npm start