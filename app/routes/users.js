const router=require('express').Router();
const UsersController =require('../controllers/usersController');


router.get('/', UsersController.apiGetAllUsers);

router.post('/', UsersController.apiCreateUser);

router.post('/login', UsersController.apiUserLogin);

router.get('/verifyToken', UsersController.apiVerifyToken);

module.exports=router;