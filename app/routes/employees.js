const jwt=require("jsonwebtoken");
const router=require('express').Router();
const EmployeeController=require('../controllers/employeeController');
const {ACCESS_TOKEN_SECRET}=require('../const');

router.post("/", EmployeeController.apiCreateEmployee);
router.get("/", EmployeeController.apiGetEmployee);
router.delete('/', EmployeeController.apiDeleteEmployee);
router.put('/', EmployeeController.apiUpdateEmployee);
router.get('/filter', EmployeeController.apiFilterEmployee);
router.get('/group', EmployeeController.apiGroupEmployee);


function authenticateToken(req, res, next){
    const authHeader=req.headers['authorization'];
    const token=authHeader && authHeader.split(" ")[1];
    // console.log(token);
    if(token==null){
        return res.sendStatus(401)
    }

    jwt.verify(token, ACCESS_TOKEN_SECRET, (err, user)=>{
        if(err) return res.sendStatus(403);
        // if(user.userInfo.role!=="Admin") return res.json({error:"Admin rights required to create new user"});
        req.user=user;
        next()
    })
}


module.exports=router;