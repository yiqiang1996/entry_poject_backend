const bcrypt=require('bcrypt');
const jwt=require('jsonwebtoken');

const User=require('../models/users');
const {ACCESS_TOKEN_SECRET}=require('../const')


class UsersController{
    async apiGetAllUsers(req, res){
        try{
            const users=await User.find();
            res.json(users)
        }catch(error){
            res.status(500).json({
                error:"Get User Failed"
            });
        }
    }

    async apiCreateUser(req, res){
        try{
            const userName=req.body.username;
            let user=await User.findOne({username:userName})
            console.log(user);
            if(user){
                res.json({
                    error: "Username already registered"
                })
            }else{
                const hashedPassword=await bcrypt.hash(req.body.password, 10)
                const newUser=new User({
                    username:req.body.username,
                    password:hashedPassword,
                    name:req.body.name? req.body.name : req.body.username,
                    email:req.body.email,
                    role:req.body.role,
                    image: req.body.image? req.body.image:""
                });

                let error=newUser.validateSync();
                // console.log(error);
                if(error && error.errors){
                    res.json({
                        error:"Create User Failed"
                    })
                }else{
                    const temp=await newUser.save();
                    res.send(temp)
                }
            }
        }catch(error){
            console.log(error);
            res.status(500).json({
                error:"Create User Failed"
            })
        }
    }

    async apiDeleteUser(req, res){

    }

    async apiVerifyToken(req, res){
        const authHeader=req.headers['authorization'];
        const token=authHeader && authHeader.split(" ")[1];
        // console.log(token);
        if(token==null){
            return res.sendStatus(401)
        }
    
        jwt.verify(token, ACCESS_TOKEN_SECRET, (err, user)=>{
            if(err) return res.json({error:"Accesstoken Not Valid"});
            // if(user.userInfo.role!=="Admin") return res.json({error:"Admin rights required to create new user"});
            // req.user=user;
            res.json(user.userInfo);
        })
    }

    async apiUserLogin(req, res){
        const userName=req.body.username;

        let user=await User.findOne({username: userName});

        if(!user){
            return res.json({
                error:"User Not Registered"
            })
        }


        try{
            if(await bcrypt.compare(req.body.password, user.password)){
                const payload={
                    username:user.name,
                    userInfo:{
                        name:user.name,
                        email:user.email,
                        role:user.role,
                        image:user.image,
                    }
                }
                const accessToken=generateAccessToken(payload);
                res.json({
                    accessToken:accessToken,
                    ...payload,
                })
            }else{
                res.json({
                    error:"Wrong Password"
                })
            }
        }catch(err){
            console.log(err);
            res.status(500).send("Server Error")
        }
    }
}

function generateAccessToken(user){
    return jwt.sign(user, ACCESS_TOKEN_SECRET, {expiresIn:'1h'});
}

module.exports=new UsersController();