const jwt = require("jsonwebtoken");
const Employee = require("../models/employees");
const moment=require('moment');

class EmployeeController {
  async apiGetEmployee(req, res) {
    try {
      const page = parseInt(req.query.page);
      const pageSize = parseInt(req.query.pageSize);
      let skip = page ? (page - 1) * pageSize : 0;
      let limit = pageSize ? pageSize : 10;

      const counts = await Employee.countDocuments();
      const data = await Employee.find().skip(skip).limit(limit);

      res.json({
        total: counts,
        data: data,
        current: page ? page : 1,
        pageSize: pageSize ? pageSize : 10,
      });
    } catch (error) {
      console.log(error);
      res.sendStatus(500);
    }
  }

  async apiFilterEmployee(req, res) {
    const name = req.query.name;
    const job = req.query.job;
    const start_date = req.query.start_date;
    const end_date = req.query.end_date;

    let page = req.query.page ? parseInt(req.query.page) : 1;
    let pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;

    // const skip = (page - 1) * pageSize;
    // const limit = pageSize;

    //construct the filter condition
    const filter = {};
    if (name) {
      filter["name"] = { $regex: ".*" + name + ".*" };
    }

    if (job) {
      filter["job"] = job;
    }

    if (start_date && end_date) {
      filter["entry_date"] = {
        $gte: start_date,
        $lte: end_date,
      };
    }

    console.log(filter);
    try {
      let total = await Employee.where(filter).countDocuments();
      page=Math.ceil(total/pageSize)>=page? page: Math.ceil(total/pageSize)>0? Math.ceil(total/pageSize):1;
      const skip=(page-1)*pageSize;
      const limit=pageSize; 
      let tempData = await Employee.find(filter).skip(skip).limit(limit);
      res.json({
        total: total,
        data: tempData,
        current: page,
        pageSize: pageSize,
      });
    } catch (error) {
      console.log(error);
      res.sendStatus(500);
    }
  }

  async apiDeleteEmployee(req, res) {
    try {
      let id = req.body._id;
      let temp = await Employee.findOneAndDelete({ _id: id });
      res.json(temp);
    } catch (error) {
      console.log(error);
      res.sendStatus(500);
    }
  }

  async apiUpdateEmployee(req, res) {
    try {
      const id = req.body._id;
      const name = req.body.name;
      let temp = await Employee.findByIdAndUpdate(
        id,
        { name: name },
        { new: true }
      );
      res.json(temp);
    } catch (error) {
      console.log(error);
      res.sendStatus(500);
    }
  }

  async apiCreateEmployee(req, res) {
    // if(req.user.userInfo.role!=='Admin') return res.json({error:"Admin rights required to create new user"});
    const employees = req.body.data;

    const validEmployees = [];

    for (let i = 0; i < employees.length; i++) {
      let employee = new Employee(employees[i]);
      let error = employee.validateSync();
      if (!error) {
        validEmployees.push(employee.save());
      }
    }

    Promise.all(validEmployees).then((values) => {
    //   console.log(values);
      res.json({
        data: values,
        total: values.length,
      });
    }).catch(err=>{
        res.sendStatus(500);
    });
  }

  async apiGroupEmployee(req, res) {
    const year = parseInt(req.query.year);
    const groupBy = req.query.groupBy;
    const months = [
      "Jan",
      "Fed",
      "March",
      "April",
      "May",
      "June",
      "July",
      "Agust",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];

    const jobs=[
         "Front-End Dev",
         "Core Server",
         "Product Manager",
         "Project Manager",
         "QA",
         "Dev OPS"
    ]


    const getYears=await Employee.aggregate([
        {$project:{year:{$year:"$entry_date"}}},
        {$group:{_id:"$year"}},
        {$sort:{_id: 1}}
    ]).exec()

    const years=[];
    for(let year of getYears){
        years.push(year._id);
    }

    if(!groupBy||groupBy==='month'){
        const groupByMonth = [];
        for(let i=0;i<months.length;i++){
            groupByMonth.push({
                _id:months[i],
                count:0
            })
        }
        const aggregate=await Employee.aggregate([
            {$project:{name:1, job:1, month:{$month:"$entry_date"}, year:{$year:"$entry_date"}}},
            {$match:{year: year}},
            {$group:{_id:"$month", count:{$sum: 1}}}
        ]).exec()

        for(let ele of aggregate){
            let index=ele._id-1;
            let count=ele.count;

            groupByMonth[index].count=count;
        }

        res.json(
            {
                data:groupByMonth,
                years:years
            });
    }else if(groupBy==="job"){
        const byJob= await Employee.aggregate([
            {$project:{name:1, job:1, year:{$year:"$entry_date"}}},
            {$match:{year: year}},
            {$group:{_id: "$job", count:{$sum:1}}}
        ]).exec();

        const groupByJob=[];
        for(let job of jobs){
            groupByJob.push({
                _id:job,
                count:0
            })
        }

        for(let result of byJob){
            let index=groupByJob.findIndex(item=>item._id===result._id);
            groupByJob[index].count=result.count;
        }

        res.json({
            data:groupByJob,
            years:years
        })
    }
  }
}

module.exports = new EmployeeController();
