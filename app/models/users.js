const mongoose=require('mongoose');
const Schema=mongoose.Schema;


const userSchema = new Schema({
    username:{
        type:String,
        required: true
    },

    password:{
        type:String,
        required: true
    },

    name:{
        type:String,
        required:true
    },

    email:{
        type:String,
        required:true
    },

    role:{
        type: String,
        enum:{
            values:['Admin','Normal'],
            message:'{VALUE} is not supported'
        },
        required:true
    },
    image:{
        type:String
    }
});

module.exports=User=mongoose.model('User', userSchema);