const mongoose=require("mongoose")
const Schema=mongoose.Schema;

const employeeSchema=new Schema({
    name:{
        type:String,
        required:true,
    },

    job:{
        type:String,
        enum:{
            values:["Front-End Dev", "Core Server", "Product Manager","Project Manager","QA", "Dev Ops"],
            message:'{VALUE} is not supported'
        },
        requied: true,
    },

    entry_date:{
        type:Date,
        required:true
    }
})

module.exports=Employee=mongoose.model("Empployee", employeeSchema);