require('dotenv').config()
const express=require('express');
const app=express();
const bcrypt=require('bcrypt');
const jwt=require("jsonwebtoken");
const cors=require("cors");
const path=require("path");
const bodyParser=require('body-parser');

const mongoose=require("mongoose");
let userRoutes=require('./routes/users');
let employeeRoutes=require('./routes/employees');

const connectionURI="mongodb://localhost:27017/test?readPreference=primary&directConnection=true&ssl=false";
mongoose.connect(connectionURI, {useNewUrlParser:true, useUnifiedTopology:true})
.then(res=> console.log('Successfully connect to mogodb'))
.catch(err=>console.log(`Error in DB Connections ${err}`))


app.use(cors({
    origin:"*",
    credentials:true
}))

app.use(express.static(path.join(__dirname,'public')));

app.use(express.json());
// app.use(bodyParser.urlencoded({extended:true}));
// app.use(bodyParser.json());

app.use('/users', userRoutes);
app.use('/employees', employeeRoutes);

app.use('/*', function(req, res){
    res.redirect('/')
})

 
app.listen(3030);
